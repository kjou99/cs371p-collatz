// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(200, 100)), make_tuple(200, 100, 125));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(10, 10)), make_tuple(10, 10, 7));
}

TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(10, 50)), make_tuple(10, 50, 112));
}

TEST(CollatzFixture, eval7) {
    ASSERT_EQ(collatz_eval(make_pair(26, 50)), make_tuple(26, 50, 112));
}

TEST(CollatzFixture, eval8) {
    ASSERT_EQ(collatz_eval(make_pair(50000, 90000)), make_tuple(50000, 90000, 351));
}

// ----
// calc
// ----

TEST(CollatzFixture, calc0) {
    ASSERT_EQ(collatz_calc(1), 1);
}

TEST(CollatzFixture, calc1) {
    ASSERT_EQ(collatz_calc(8), 4);
}

TEST(CollatzFixture, calc2) {
    ASSERT_EQ(collatz_calc(10), 7);
}

TEST(CollatzFixture, calc3) {
    ASSERT_EQ(collatz_calc(27), 112);
}

TEST(CollatzFixture, calc4) {
    ASSERT_EQ(collatz_calc(77031), 351);
}

TEST(CollatzFixture, calc5) {
    ASSERT_EQ(collatz_calc(837799), 525);
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}

TEST(CollatzFixture, solve1) {
    istringstream sin("50000 90000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "50000 90000 351\n");
}

TEST(CollatzFixture, solve2) {
    istringstream sin("1 1000000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 1000000 525\n");
}

TEST(CollatzFixture, solve3) {
    istringstream sin("1 1\n10 1\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 1 1\n10 1 20\n");
}



