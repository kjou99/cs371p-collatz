// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

using namespace std;

long lazy[1000000] = {};
long meta[1000] = {};

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);}

// ------------
// collatz_calc
// ------------

long collatz_calc (long i) {
    long store = i;
    assert(i > 0);
    if (lazy[i]) {
        return lazy[i];
    }
    int counter = 1;
    while (i > 1) {
        if ((i % 2) == 0) {
	    i = i / 2;
	}
	else {
	    i = (3*i) + 1;
	    assert((i % 2) == 0);
	    i = i / 2; //first optimization trick from quiz
	    ++counter;
	}
	++counter;
    }
    assert(counter > 0);
    assert(store < 1000000);
    lazy[store] = counter;
    if (store < 500000) lazy[2*store] = counter + 1;
    if (store < 250000) lazy[4*store] = counter + 2;
    return counter;
}

// -----------
// cache_build
// -----------

void cache_build() {
    for (long i = 1; i < 500000; i++) {
        collatz_calc(i);
    }
}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i, j;
    tie(i, j) = p;
    // <your code>
    int lower, upper, m, curr, max;
    max = 0;
    curr = 0;
    if (i > j) {
        lower = j;
	upper = i;
    }
    else {
        lower = i;
	upper = j;
    }
    m = (upper / 2) + 1;
    if (m > lower) lower = m;
    //cout << "lower " << lower << " upper " << upper << endl;
    for (int x = lower; x <= upper; x++) {
	if (x + 2000 <= upper) {
	    assert((x/1000) < 1000);
	    curr = meta[x/1000];
	    if (curr) {
	    	x = ((x / 1000) * 1000) + 999;
	    	assert(x <= upper);
	    	if (curr > max) {
	        	max = curr;
			//cout << "new max " << x << " from meta" << endl;
	    	} //else cout << "not max " << x << endl;
	    	continue;
	    }
	}
        curr = collatz_calc(x);
	if (curr > max) {
	    max = curr;
	    //cout << "new max " << x << endl;
	}
    }
    return make_tuple(i, j, max);}

// ----------
// meta_cache
// ----------

void meta_cache() {
    for (int i = 0; i < 1000; i++) {
        meta[i] = get<2>(collatz_eval(make_pair(i* 1000, (i*1000) + 999)));
    }
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    cache_build();
    meta_cache();
    string s;
    while (getline(sin, s))
         collatz_print(sout, collatz_eval(collatz_read(s)));}

int main () {
    collatz_solve(cin, cout);
    return 0;}

